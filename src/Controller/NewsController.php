<?php

namespace App\Controller;


use App\Controller\BaseController\FrontController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\News;
use Symfony\Component\HttpFoundation\Request;
use App\Component\Pagination;




class NewsController extends FrontController
{
    /**
     * @Route("/news", name="news")
     */
    public function index()
    {
        //$request = new Request(['page');
        $request = Request::createFromGlobals();
        $page = $request->query->get('page');
        $repository = $this->getDoctrine()->getRepository(News::class);
        $pagination = new Pagination($repository->count([]), (int)$page);
        $pagination->init();
        $newsList = $repository->findByPublicationDtLess(date('Y-m-d H:i:s'), $pagination->getFirstItemOnPage(), 3);

        return $this->render('news/index.html.twig', [
            'title' => 'Новости',
            'newsList' => $newsList,
            'pagination' => $pagination,
        ]);
    }
    /**
     * @Route("/news/{slug}", name="news_view")
     */
    public function view($slug)
    {
        $repository = $this->getDoctrine()->getRepository(News::class);
        $news =  $repository->findOneBy(['slug' => $slug]);
        //var_dump($news->getTitle());


        return $this->render('news/view.html.twig', [
            'news' => $news,
        ]);
    }
}
