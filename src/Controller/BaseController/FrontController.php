<?php

namespace App\Controller\BaseController;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class FrontController extends AbstractController
{

    public function render(string $view, array $parameters = [], Response $response = null)
    : Response
    {
        $parameters ['__view']       = $view;
        $parameters ['__parameters'] = $parameters;
        $view                        = 'layouts/base_page.html.twig';

        return parent::render($view, $parameters);
    }


}