<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 */
class News
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $create_dt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $publication_dt;

    /**
     * @ORM\Column( name="slug" , unique=true,  length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $short_text;

    /**
     * @ORM\Column(type="text")
     */
    private $text;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreateDt()
    {
        return $this->create_dt;
    }

    public function setCreateDt( $create_dt): self
    {
        $this->create_dt = $create_dt;

        return $this;
    }

    public function getPublicationDt()
    {
        return $this->publication_dt;
    }

    public function setPublicationDt( $publication_dt): self
    {
        $this->publication_dt = $publication_dt;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getShortText(): ?string
    {
        return $this->short_text;
    }

    public function setShortText(string $short_text): self
    {
        $this->short_text = $short_text;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }
}
