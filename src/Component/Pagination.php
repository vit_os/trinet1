<?php

namespace App\Component;

use Symfony\Component\HttpFoundation\Response;

class Pagination
{
    private $countItems;
    private $currentPage;
    private $itemsOnPage = 3;
    private $pages;
    private $firstItemOnPage;

    public function __construct($contItems, $currentPage)
    {
        $this->countItems  = $contItems;
        $this->currentPage = $currentPage;
    }

    public function init()
    {
        $this->pages           = (int)ceil($this->countItems / $this->itemsOnPage);
        $this->firstItemOnPage = $this->itemsOnPage * $this->currentPage;
    }

    public function getFirstItemOnPage()
    {
        return $this->firstItemOnPage;
    }

    public function getNumPages()
    {
        return $this->pages;
    }

    public function getCurrentPage()
    {
        return  $this->currentPage;
    }

}